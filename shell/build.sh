#!/bin/bash

cd ~/hw-6

docker service rm web_app_test web_app_prod web_app
docker build --build-arg SECRET_NUMBER=$1 -t web_app_image . 
docker tag web_app_image 65.21.241.36:5050/web_app_image 
docker push 65.21.241.36:5050/web_app_image

cd -
